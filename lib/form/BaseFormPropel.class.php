<?php

/**
 * Project form base class.
 *
 * @package    jobeet
 * @subpackage form
 * @author     Ruslan Z
 */
abstract class BaseFormPropel extends sfFormPropel
{
  public function setup()
  {
  }
}
