<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ZRR
 * Date: 13.02.13
 * Time: 0:05
 * To change this template use File | Settings | File Templates.
 */
class Jobeet
{
    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('#[^\\pL\d]+#u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv'))
        {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }
        $text = preg_replace('/\W+/', '-', $text);
        $text = strtolower(trim($text, '-'));
        if (empty($text))
        {
            return 'n-a';
        }
        return $text;
    }


}
