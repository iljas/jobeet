<?php

/**
 * Project filter form base class.
 *
 * @package    jobeet
 * @subpackage filter
 * @author     Ruslan Z
 */
abstract class BaseFormFilterPropel extends sfFormFilterPropel
{
  public function setup()
  {
  }
}
